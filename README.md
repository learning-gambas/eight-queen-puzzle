# Eight-Queen Puzzle in GAMBAS

This is a visualization of the eight-queen puzzle listing I saw in the "**Programming in Lua**" book by *Roberto Lerusalimschy*

Of importance for me was an understanding, in a visual way (as I'm a visual learner), how the backtracking process worked.

I've invested close to 2 days in this and I'm at a point where I feel I've done enough to help myself and hopefully others understand how this puzzle is solved using the recursive method with backtracking.

I leave it to anyone in the future who would like to modify this so that it uses a dynamic layout generator so that is can be used to simulate a board of any size.

Here's a GIF showing the running program:

![GIF of puzzle in operation](./images/puzzle.gif "Puzzle Operation")

